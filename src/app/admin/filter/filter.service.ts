import { Injectable } from '@angular/core';
import { HttpModule, Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../../app.constant';

@Injectable()
export class FilterService {

    constructor(public http: Http, public _config: Configuration) { }

    addFilter(val) {
        let url = this._config.Server + 'filter';
        return this.http.post(url, JSON.stringify(val), { headers: this._config.headers }).map(res => res.json());
    }
    getData() {
        let url = this._config.Server + 'filter';
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getLanguage() {
        let url = this._config.Server + 'language';
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }

    editFilter(val) {
        let url = this._config.Server + 'filter';
        return this.http.put(url, JSON.stringify(val), { headers: this._config.headers }).map(res => res.json());
    }
    deleteFilter(filterId) {
        let url = this._config.Server + 'filter?filterId=' + filterId;
        return this.http.delete(url, { headers: this._config.headers }).map(res => res.json());
    }




}