import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit } from '@angular/core';
import { AppConfig } from "../../app.config";
import { Router } from '@angular/router';
import { PushMarketingService } from './pushMarketing.service';
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { PagesComponent } from '../../pages/pages.component';

declare var swal: any;
@Component({
    selector: 'pushMarketing',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./pushMarketing.component.scss'],
    templateUrl: './pushMarketing.component.html',
    providers: [PushMarketingService]
})

export class PushMarketingComponent {
    public rowsOnPage = 10;
    public p = 1;
    public searchEnabled = 0;
    public searchTerm = '';
    msg: any = false;
    count: any;
    detailData: any;
    data = [];
    campaignInfo = {};
    btnFlag = true;


    constructor(private _appConfig: AppConfig, private _service: PushMarketingService, private router: Router, vcRef: ViewContainerRef, public _isAdmin: PagesComponent) { }

    ngOnInit() {
        if (this._isAdmin.isAdmin == false) {
            var role = sessionStorage.getItem('role');
            var roleDt = JSON.parse(role);
            for (var x in roleDt) {
                if (x == 'campaign') {
                    if (roleDt[x] == 0) {
                        this.router.navigate(['error']);
                    } else if (roleDt[x] == 100) {
                        jQuery(".runCampBtn").hide();
                        jQuery(".thAction").remove();
                        this.btnFlag = false;
                    }
                }
            }
        }

        this._service.getAllCampaign().subscribe(
            res => {
                // console.log("res", res);
                if (res.data && res.data.length > 0) {
                    this.msg = false;
                    this.data = res.data;
                } else {
                    this.data = [];
                    this.msg = true;
                }
            }
        )

    }
    getcampaignInfo(id) {
        this._service.getcampaignInfo(id).subscribe(
            res => {
                // console.log("res", res);                
                if (res.code == 200) {
                    this.campaignInfo = res.data[0];
                    // console.log("res", res);
                    jQuery('#template').modal('show');
                }

            }
        )
    }
    gotoDelete(id) {
        this._service.deleteCampaign(id).subscribe(
            res => {
                if (res.code == 200) {
                    swal("Success!", "Deleted Successfully!", "success");
                    this.ngOnInit();
                }
            }
        )
    }
    launchNewCamp() {
        this.router.navigate(['/app/campaign/launch-new-campaign']);
    }
    closeModal() {
        jQuery('#template').modal('hide');
    }
    gotoTargetedUser(id) {
        this.router.navigate(['/app/campaign/targeted-user', id]);
    }
    gotoViewUser(id) {
        this.router.navigate(['/app/campaign/view', id]);
    }
    gotoClickUser(id) {
        this.router.navigate(['/app/campaign/click', id]);
    }
}