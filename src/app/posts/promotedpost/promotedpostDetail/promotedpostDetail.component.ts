import { Component, OnInit, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit } from '@angular/core';
import { AppConfig } from "../../../app.config";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PromotedPostService } from '../promotedpost.service';

import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { PagesComponent } from '../../../pages/pages.component';


@Component({
    selector: 'promotView',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./promotedpostDetail.component.scss'],
    templateUrl: './promotedpostDetail.component.html',
    providers: [PromotedPostService]
})
export class promotedpostDetailComponent implements OnInit {
    sub: any;
    postId: any;
    public rowsOnPage = 10;
    public p = 1;
    msg = false;
    offerDetail = [];
    pramotPostData:any;
    count:any
    detailData:any;

    constructor(private route: ActivatedRoute, private _service: PromotedPostService, private router: Router, vcRef: ViewContainerRef, public _isAdmin: PagesComponent) {

    }
    ngOnInit() {
        // if (this._isAdmin.isAdmin == false) {
        //     var role = sessionStorage.getItem('role');
        //     var roleDt = JSON.parse(role);
        //     for (var x in roleDt) {
        //         if (x == 'active-post') {
        //             if (roleDt[x] == 0) {
        //                 this.router.navigate(['error']);
        //             }
        //         }
        //     }
        // }

        this.sub = this.route.params.subscribe(params => {
            this.postId = params['postId'];
        });
        this.p =1;
        this.PromotedUserDetail(this.p);
    }
    PromotedUserDetail(p) {
        this._service.getPromotedUserDetail(this.postId,p - 1, this.rowsOnPage).subscribe(
            result => {
                    if (result.data && result.data.length > 0) {
                        this.msg = false;
                        // console.log("+++++++result",result.count[0].count)
                        this.count=result.count[0].count
                        this.pramotPostData = result.data;
                        // console.log("+++++++",this.pramotPostData)

                    } else {
                        // this.postData = [];
                        // this.msg = "No data Available";
                    }
            }
        )
    }
    gotoUserDatail(user) {
        // console.log("user",user)
        this._service.getUserDetail(user).subscribe(
            result => {
                if (result.code == 200) {
                    this.detailData = result.data;
                    jQuery('#userDetail').modal('show');
                }
            }
        )
    }

}