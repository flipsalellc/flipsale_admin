import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit } from '@angular/core';
import { AppConfig } from "../../app.config";
import { Router } from '@angular/router';
import { CreatePostService } from './createpost.service';
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { PagesComponent } from '../../pages/pages.component';
import { ResourceLoader } from '@angular/compiler';
import { THROW_IF_NOT_FOUND } from '@angular/core/src/di/injector';
declare var CLOUDNAME: string, CLOUDPRESET: string;
declare var swal: any;
declare var sweetAlert: any;
@Component({
    selector: 'createpost',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./createpost.component.scss'],
    templateUrl: './createpost.component.html',
    providers: [CreatePostService]
})

export class CreatePostComponent {
    createPost: FormGroup;
    editPost: FormGroup;
    public autocomplete: any;
    public editautocomplete: any;
    public place: any;
    public lat: any;
    public long: any;
    public location: any;
    public editLocation: any;
    public Feature = "True";
    public negotiable = '';
    logoUrl: any;
    public searchEnabled = 0;
    public searchTerm = '';
    category = '';
    subCategory = '';
    userlist: any;
    user_value: any;
    user_category: any;
    cloudName: any;
    preset: any;
    public url: any;
    public data: any;
    type: string = '0';
    imageId: any;
    postId: any
    public msg: any = false;
    public rowsOnPage = 10;
    public p = 1;
    adminPost: any;
    count: any;
    state = 1;
    editPostDetail: any;
    detailData: any;
    errorImg = false;
    filter: any = 0;
    obj = { name: 'as' };
    image: any = '';
    btnFlag = true;
    subCat: any;
    subCatData: any;
    filterData: any;
    subCatSelect = '';
    containerHeight: any;
    containerWidth: any;

    cloudinaryImage: any;
    uploader: CloudinaryUploader = new CloudinaryUploader(
        new CloudinaryOptions({ cloudName: CLOUDNAME, uploadPreset: CLOUDPRESET })
    );

    imageUploader = (item: any, response: string, status: number, headers: any) => {
        let cloudinaryImage = JSON.parse(response);
        this.containerHeight = cloudinaryImage.height;
        this.containerWidth = cloudinaryImage.width;
        this.url = cloudinaryImage.url;
        if (this.url) {
            jQuery(".loader").hide();
        }
    };
    constructor(private _appConfig: AppConfig, private _createpostservice: CreatePostService, private router: Router, vcRef: ViewContainerRef, fb: FormBuilder, public _isAdmin: PagesComponent) {
        this.createPost = fb.group({
            'Feature': "",
            'membername': ['', Validators.required],
            'mainUrl': "",
            'category': "",
            'subCategory': "",
            'productName': ['', Validators.required],
            'description': ['', Validators.required],
            'price': ['', Validators.required],
            'negotiable': "",
            'location': "",
            'latitude': "",
            'longitude': "",

        });
        this.editPost = fb.group({
            'Feature': "",
            'membername': ['', Validators.required],
            'mainUrl': "",
            'category': "",
            'subCategory': "",
            'productName': ['', Validators.required],
            'description': ['', Validators.required],
            'price': ['', Validators.required],
            'negotiable': "",
            'location': "",
            'latitude': "",
            'longitude': "",

        });
    }
    upload() {
        this.uploader.uploadAll();
        this.errorImg = false;
        this.uploader.uploadAll();
        jQuery(".loader").show();
    }
    ngOnInit() {
        jQuery(".loader").hide();

        if (this._isAdmin.isAdmin == false) {
            var role = sessionStorage.getItem('role');
            var roleDt = JSON.parse(role);
            for (var x in roleDt) {
                if (x == 'active-post') {
                    if (roleDt[x] == 0) {
                        this.router.navigate(['error']);
                    } else if (roleDt[x] == 100) {
                        this.btnFlag = false; jQuery(".thAction").remove();
                        jQuery(".thTitle").css("width", "39%");
                        jQuery("#addBtn").hide();
                    } else if (roleDt[x] == 110) {
                        jQuery("#addBtn").hide(); jQuery(".thAction").remove();
                        jQuery(".thTitle").css("width", "39%");
                        this.btnFlag = false;
                    }
                }
            }
        }

        this.uploader.onSuccessItem = this.imageUploader;
        jQuery("#userdata_parent").hide();
        jQuery("#edituserdata_parent").hide();
        this.autocomplete = new google.maps.places.Autocomplete((<HTMLInputElement>document.getElementById('location')));
        this.autocomplete.addListener('place_changed', () => {
            this.place = this.autocomplete.getPlace();
            this.location = this.place.name;
            this.lat = this.place.geometry.location.lat();
            this.long = this.place.geometry.location.lng();
            // console.log("name", this.location);
            (<HTMLInputElement>document.getElementById('latitude')).value = this.place.geometry.location.lat();
            (<HTMLInputElement>document.getElementById('longitude')).value = this.place.geometry.location.lng();
            jQuery("input#location").val(this.place.name);
        });
        this.editautocomplete = new google.maps.places.Autocomplete((<HTMLInputElement>document.getElementById('editLocation')));
        this.editautocomplete.addListener('place_changed', () => {
            this.place = this.editautocomplete.getPlace();
            this.editLocation = this.place.name;
            this.lat = this.place.geometry.location.lat();
            this.long = this.place.geometry.location.lng();
            (<HTMLInputElement>document.getElementById('editLatitude')).value = this.place.geometry.location.lat();
            (<HTMLInputElement>document.getElementById('editLongitude')).value = this.place.geometry.location.lng();
            jQuery("input#editLocation").val(this.place.name);
        });

        // this.gotoGetCategory();
        this._createpostservice.getuserCategory().subscribe(
            result => {
                this.user_category = result.data;
                console.log("result", result);
                this.user_category.forEach(e => {
                    if (e.subCategoryCount == 0) {
                        if (e.filter.length != 0) {
                            e.filter.forEach(ele => {
                                if ((ele.type == 2 || ele.type == 4 || ele.type == 6) && ele.values) {
                                    ele.values = ele.values.split(',');
                                }
                            });
                        }
                    }
                });
            }
        )
        this.getPage();

    }

    getPage() {
        if (this.filter == 1) {
            var category = jQuery("#categoryPost").val();
        } else if (this.filter == 2) {
            var category = jQuery("#categoryPost").val();
            var subCategory = jQuery("#subCategoryPost").val();
        }
        this._createpostservice.getAdminPost(category, subCategory, this.filter, this.searchEnabled, this.searchTerm).subscribe(
            result => {
                // console.log("result", result);
                if (result != null && result.data.length > 0) {
                    // alert(1);
                    this.msg = false;
                    this.adminPost = result.data;
                } else {
                    this.adminPost = [];
                    this.msg = "No Post available";
                }
            }
        )
    }
    getPageOnSearch(term) {
        // console.log("search", term);
        // console.log("search model", this.searchTerm);
        this.searchTerm = term;
        if (this.searchTerm.length > 0) {
            this.searchEnabled = 1;
        } else {
            this.searchEnabled = 0;
        }
        this.getPage();

    }
    fileChange(input) {
        const reader = new FileReader();
        if (input.files.length) {
            const file = input.files[0];
            reader.onload = () => {
                this.image = reader.result;
            }
            reader.readAsDataURL(file);
        }
    }
    removeImage(): void { this.image = ''; }
    gotoUserSearch(term) {

        // console.log("search", term);
        // console.log("search model", this.searchTerm);
        this.searchTerm = term;
        if (this.searchTerm.length > 0) {
            this.searchEnabled = 1;
        } else {
            jQuery("#userdata_parent").hide();
            jQuery("#edituserdata_parent").hide();
            this.searchEnabled = 0;
        }
        this._createpostservice.getusername(this.searchEnabled, term).subscribe(
            result => {
                this.userlist = result.data;
                // console.log("result", result);
                jQuery("#userdata_parent").show();
                jQuery("#edituserdata_parent").show();
            }
        )
    }
    Submit(value): void {
        // console.log("this.subCatData", this.subCatData);
        value._value.location = jQuery("input#location").val();
        value._value.latitude = jQuery("input#latitude").val();
        value._value.longitude = jQuery("input#longitude").val();
        value._value.mainUrl = this.url;
        value._value.thumbnailImageUrl = this.url;
        value._value.imageCount = "1";
        value._value.condition = "new";
        value._value.containerHeight = this.containerHeight;
        value._value.containerWidth = this.containerWidth;
        value._value.currency = "USD";
        value._value.type = this.type;
        var filData = {};
        if (value._value.category && value._value.subCategory) {
            this.subCatData.forEach(e => {
                if ((e.subCategoryName == value._value.subCategory) && e.filter.length != 0) {
                    e.filter.forEach(ele => {
                        if (ele.type == 3 || ele.type == 5) {
                            filData[ele.fieldName] = parseInt(jQuery('.filterId' + ele.id).val());
                        } else {
                            filData[ele.fieldName] = jQuery('.filterId' + ele.id).val();
                        }
                    });
                }
            });
        } else if (value._value.category) {
            this.user_category.forEach(e => {
                if ((e.name == value._value.category) && e.filter.length != 0) {
                    e.filter.forEach(ele => {
                        if (ele.type == 3 || ele.type == 5) {
                            filData[ele.fieldName] = parseInt(jQuery('.filterId' + ele.id).val());
                        } else {
                            filData[ele.fieldName] = jQuery('.filterId' + ele.id).val();
                        }
                    });
                }
            });
        }
        value._value.filter = filData;
        console.log("value._value", value._value)
        // console.log("ddd", filData);
        // this.state = 1;
        // console.log("image url", this.url);
        // console.log("data", this.url);
        // jQuery('#create-modal').modal('hide');
        // this.ngOnInit();
        // this.createPost.reset();
        if (this.url.length > 0) {
            this.msg = false;
            this._createpostservice.submitPost(value._value).subscribe(
                result => {
                    console.log("result", result);
                    if (result.code == 200) {
                        swal("Success", "Post Added Successfully!", "success");
                        jQuery('#create-modal').modal('hide');
                        this.ngOnInit();
                        this.filterData = '';
                        this.createPost.reset();
                    } else {
                        sweetAlert("Oops...", "Something went wrong!", "error");
                    }

                }
            )
        } else {
            this.msg = true;
        }


    }
    submitData(url) {
        this.data.mainUrl = url;
        this.data.thumbnailImageUrl = url;
        this.data.imageCount = "1"; this.data.condition = "new"; this.data.containerHeight = "1024";
        this.data.containerWidth = "768"; this.data.currency = "USD";
        this.data.type = this.type;
        // console.log("value", this.data);
        this._createpostservice.submitPost(this.data).subscribe(
            result => {
                // console.log("result", result);
                if (result.code == 200) {
                    this.url = '';
                    swal("Good job!", "Post Added Successfully!", "success");
                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }
                jQuery('#create-modal').modal('hide');
                this.ngOnInit();
                this.createPost.reset();
            }
        )
    }
    editPostData(post) {
        console.log("post", post);
        this.obj = post;
        this.url = post.mainUrl;
        this.postId = post.postId;
        this.category = post.category;
        this.subCatSelect = post.subCategory;
        this.containerHeight = post.containerHeight;
        this.containerWidth = post.containerWidth;
        this._createpostservice.getSubcategory(this.category).subscribe(
            res => {
                if (res.code == 200) {
                    this.subCatData = res.data;
                    this.subCatData.forEach(e => {
                        if (e.filter.length != 0) {
                            e.filter.forEach(ele => {
                                if ((ele.type == 2 || ele.type == 4 || ele.type == 6) && ele.values) {
                                    ele.values = ele.values.split(',');
                                }
                            });
                        }
                    });
                    this.gotoSubcategory();

                } else {
                    this.subCatData = '';
                }
            }
        )

        this.subCategory = post.subCategory;
        this.negotiable = post.negotiable;
        this.logoUrl = post.mainUrl;
    }
    updatePost(value) {

        value._value.location = jQuery("input#editLocation").val();
        value._value.latitude = jQuery("input#editLatitude").val();
        value._value.longitude = jQuery("input#editLongitude").val();
        value._value.postId = this.postId;
        value._value.imageCount = "1"; value._value.condition = "new"; value._value.containerHeight = this.containerHeight;
        value._value.containerWidth = this.containerWidth; value._value.currency = "USD";
        value._value.type = this.type;
        value._value.postType = 0;
        value._value.mainUrl = this.url;

        var filData = {};
        if (value._value.category && value._value.subCategory) {
            this.subCatData.forEach(e => {
                if ((e.subCategoryName == value._value.subCategory) && e.filter.length != 0) {
                    e.filter.forEach(ele => {
                        if (ele.type == 3 || ele.type == 5) {
                            filData[ele.fieldName] = parseInt(jQuery('.filterEdit' + ele.id).val()) || '';
                        } else {
                            filData[ele.fieldName] = jQuery('.filterEdit' + ele.id).val() || '';
                        }
                    });
                }
            });
        } else if (value._value.category) {
            this.user_category.forEach(e => {
                if ((e.name == value._value.category) && e.filter.length != 0) {
                    e.filter.forEach(ele => {
                        if (ele.type == 3 || ele.type == 5) {
                            filData[ele.fieldName] = parseInt(jQuery('.filterEdit' + ele.id).val());
                        } else {
                            filData[ele.fieldName] = jQuery('.filterEdit' + ele.id).val();
                        }
                    });
                }
            });
        }
        value._value.filter = filData;
        console.log("value._value", value._value);

        this._createpostservice.updatepost(value._value).subscribe(
            result => {
                // console.log("result", result);
                jQuery('#edit-modal').modal('hide');
                this.ngOnInit();
                this.createPost.reset();
            }
        )
    }
    // editDataPost(url) {
    //     // alert(4);
    //     this.editPostDetail.mainUrl = url;
    //     this._createpostservice.updatepost(this.editPostDetail).subscribe(
    //         result => {
    //             // console.log("result", result);
    //             jQuery('#edit-modal').modal('hide');
    //             this.ngOnInit();
    //             this.createPost.reset();
    //         }
    //     )
    // }

    gotoGetCategory() {
        this.subCatData = '';
        this.filterData = '';
        this.user_category.forEach(e => {
            if (e.name == this.category) {
                if (e.subCategoryCount == 0) {
                    this.subCatData = '';
                    this.filterData = e.filter;
                } else {
                    this._createpostservice.getSubcategory(this.category).subscribe(
                        res => {
                            console.log("res", res);
                            this.filterData = '';
                            if (res.code == 200) {
                                this.subCatData = res.data;
                                this.subCatData.forEach(e => {
                                    if (e.filter.length != 0) {
                                        e.filter.forEach(ele => {
                                            if ((ele.type == 2 || ele.type == 4 || ele.type == 6) && ele.values) {
                                                ele.values = ele.values.split(',');
                                            }
                                        });
                                    }
                                });

                            } else {
                                this.subCatData = '';
                            }
                        }
                    )
                }
            }
        });

    }
    gotoSubcategory() {
        this.filterData = '';
        this.subCatData.forEach(e => {
            if (e.subCategoryName == this.subCatSelect) {
                this.filterData = e.filter;
            }
        });
    }
    selectCat() {
        this.filter = 1;
        this.getPage();
        var category = jQuery("#categoryPost").val();
        this._createpostservice.getSubcategory(category).subscribe(
            res => {
                this.subCat = res.data;
                // console.log("subCat",this.subCat)
            }
        )
    }
    selectSubCat() {
        this.filter = 2;
        this.getPage();
    }
    clearDrop() {
        jQuery("#categoryPost").val('');
        jQuery("#subCategoryPost").val('');
        this.filter = 0;
        this.getPage();
    }

    sendUservalue(value): void {
        this.user_value = value;
        jQuery("#userdata_parent").hide();
        jQuery("#edituserdata_parent").hide();
    }
    gotoDeletePost(id) {
        var post = this;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Post!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    post.deletepost(id);
                    swal({
                        title: 'Delete!',
                        text: 'Post Deleted Successfully!',
                        type: 'success'
                    });

                } else {
                    swal("Cancelled", "Your Post is safe :)", "error");
                }
            });
    }
    deletepost(id) {
        this._createpostservice.deletePost(id).subscribe(
            result => {
                // console.log("result", result);
                this.ngOnInit();
            }
        )
    }
    gotoUserDatail(user) {
        // console.log("user",user)
        this._createpostservice.getUserDetail(user).subscribe(
            result => {
                if (result.code == 200) {
                    this.detailData = result.data;
                    jQuery('#userDetail').modal('show');
                }
            }
        )
    }
    clearModal() {
        this.createPost.reset();
        this.url = '';
    }
}